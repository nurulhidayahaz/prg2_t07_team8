﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace PRG2_T07_Team8
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();
            List<BusinessLocation> businessLocationList = new List<BusinessLocation>();
            List<SHNFacility> shnList = new List<SHNFacility>();

            DisplayMenu(personList, businessLocationList, shnList);
        }

        //Main menu
        static void DisplayMenu(List<Person> pList, List<BusinessLocation> bList,
            List<SHNFacility> sList)
        {

            int option = 1;
            while (true)
            {
                string dash = new string('-', 10);
                Console.WriteLine("{0} {1} {2}", dash, "MENU", dash);
                Console.WriteLine("[1] Load Person and Business Location data");
                Console.WriteLine("[2] Load SHN Facility data");
                Console.WriteLine("[3] List all Visitors");
                Console.WriteLine("[4] List Person Details");
                Console.WriteLine("[5] Assign/Replace TraceTogether Token");
                Console.WriteLine("[6] List all Business Locations");
                Console.WriteLine("[7] Edit Business Location Capacity");
                Console.WriteLine("[8] SafeEntry Check-In");
                Console.WriteLine("[9] SafeEntry Check-out");
                Console.WriteLine("[10] List all SHN Facilities");
                Console.WriteLine("[11] Create Visitor");
                Console.WriteLine("[12] Create TravelEntry Record");
                Console.WriteLine("[13] Calculate SHN Charges");
                Console.WriteLine("[14] Contact Tracing Reporting");
                Console.WriteLine("[15] SHN Status Reporting w/ DateTime");
                Console.WriteLine("[16] SHN Status Reporting w/ Country");
                Console.WriteLine("[0] Exit");
                Console.WriteLine("{0}{1}", dash, dash + "-------");
                Console.Write("Enter your option: ");

                string input = Console.ReadLine();
                if (Int32.TryParse(input, out _) == true)
                {
                    option = Convert.ToInt32(input);
                    if (option == 1)
                    {
                        LoadPersonData(pList);
                        LoadBusinessLocationList(bList);
                        Console.WriteLine("Data successfully loaded.");
                    }
                    else if (option == 2)
                    {
                        sList = LoadSHNFacilityData(sList);
                        Console.WriteLine("Data successfully loaded.");
                    }
                    else if (option == 3)
                    {
                        ListVisitor(pList);
                    }
                    else if (option == 4)
                    {
                        ListPerson(pList);
                    }
                    else if (option == 5)
                    {
                        AssignReplaceToken(pList);
                    }
                    else if (option == 6)
                    {
                        ListBusinessLocation(bList);
                    }
                    else if (option == 7)
                    {
                        EditBusinessLocationCapacity(bList);
                    }
                    else if (option == 8)
                    {
                        SafeEntryCheckIn(pList, bList);
                    }
                    else if (option == 9)
                    {
                        SafeEntryCheckOut(pList);
                    }
                    else if (option == 10)
                    {
                        ListSHNFacilities(sList);
                    }
                    else if (option == 11)
                    {
                        CreateVisitor(pList);
                    }
                    else if (option == 12)
                    {
                        CreateTravelEntryRecord(pList, sList);
                    }
                    else if (option == 13)
                    {
                        CalculateSHNCharges(pList);
                    }
                    else if (option == 14)
                    {
                        ContactTracingReporting(pList, bList);
                    }
                    else if (option == 15)
                    {
                        SHNStatusReporting(pList);
                    }
                    else if (option == 16)
                    {
                        SHNCountryReporting(pList);
                    }
                    else if (option == 0)
                    {
                        Console.WriteLine("Bye!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid option! Please try again.");
                    }
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Invalid option! Please try again.");
                }
            }
        }
        //Basic Features(1)
        static void LoadPersonData(List<Person> pList)
        {
            using (StreamReader sr = new StreamReader("Person.csv"))
            {
                string s = sr.ReadLine(); // heading
                while ((s = sr.ReadLine()) != null)
                {
                    string[] data = s.Split(',');

                    if (data[0] == "resident")
                    {
                        Resident r = new Resident(data[1], data[2],
                            Convert.ToDateTime(data[3]));
                        if (data[6] != "") //if resident has the token
                        {
                            r.Token = new TraceTogetherToken(data[6], data[7],
                                DateTime.Parse(data[8]));
                        }
                        if (data[9] != "") //if resident went travelling to another country
                        {
                            TravelEntry t = new TravelEntry(data[9], data[10],
                                Convert.ToDateTime(data[11]));
                            t.ShnEndDate = Convert.ToDateTime(data[12]);
                            t.IsPaid = Convert.ToBoolean(data[13]);

                            if (data[14] != "") //if resident went to a SHN facility
                            {
                                t.ShnStay = new SHNFacility();
                                t.ShnStay.FacilityName = data[14];
                            }
                            r.AddTravelEntry(t);
                        }
                        pList.Add(r);
                    }
                    else if (data[0] == "visitor")
                    {
                        Visitor v = new Visitor(data[1], data[4], data[5]);

                        if (data[9] != "") //visitor's last travelled country
                        {
                            TravelEntry t = new TravelEntry(data[9], data[10],
                                Convert.ToDateTime(data[11]));
                            t.ShnEndDate = Convert.ToDateTime(data[12]);
                            t.IsPaid = Convert.ToBoolean(data[13]);

                            if (data[14] != "") //if visitor went to a SHN facility
                            {
                                t.ShnStay = new SHNFacility();
                                t.ShnStay.FacilityName = data[14];

                            }
                            v.AddTravelEntry(t);
                        }
                        pList.Add(v);
                    }
                }
            }
        }

        static void LoadBusinessLocationList(List<BusinessLocation> bList)
        {
            using (StreamReader sr = new StreamReader("BusinessLocation.csv"))
            {
                string s = sr.ReadLine(); // heading
                while ((s = sr.ReadLine()) != null)
                {
                    string[] data = s.Split(',');
                    bList.Add(new BusinessLocation(data[0], data[1], Convert.ToInt32(data[2])));
                }
            }
        }

        //Basic Features (2)
        static List<SHNFacility> LoadSHNFacilityData(List<SHNFacility> sList)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    sList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                }
            }
            return sList;
        }

        //Basic Features(3) : Listing visitors
        static void ListVisitor(List<Person> pList)
        {
            foreach (Person p in pList)
            {
                if (p is Visitor)
                {
                    Visitor v = (Visitor)p;
                    Console.WriteLine("{0} {1} {2}",
                        v.Name, v.PassportNo, v.Nationality);
                }
            }
        }

        //Basic Features(4) : List Person Details

        static void ListPerson(List<Person> pList)
        {
            Person p = InputValidationPerson(pList);
            Console.WriteLine("Name: " + p.Name);

            foreach (TravelEntry te in p.travelEntryList)
            {
                Console.WriteLine(te.ToString());
            }
            foreach (SafeEntry se in p.safeEntryList)
            {
                Console.WriteLine(se.ToString());
            }
            if (p.GetType().Name == "Resident")
            {
                Resident r = (Resident)p;
                if (r.Token is not null)
                {
                    Console.WriteLine(r.Token.ToString());
                }                
            }
        } 

        //Basic Featues(5) : Assigning/replacing TraceTogether Token
        static void AssignReplaceToken(List<Person> pList)
        {
            while (true)
            {
                Person p = InputValidationPerson(pList);

                if (p.GetType().Name == "Resident") //search for resident in list
                {
                    Resident r = (Resident)p;
                    if (r.Token is null) //if resident has no existing token
                    {
                        r.Token = new TraceTogetherToken();
                    }
                    if (r.Token.IsEligibleForReplacement() == true) //if token is eligible for replacement
                    {
                        Console.Write("Enter token serial number: ");
                        string sNo = Console.ReadLine();
                        Console.Write("Enter token collection location: ");
                        string colLocation = Console.ReadLine();
                        r.Token.ReplaceToken(sNo, colLocation);
                    }
                    break;
                }
                else
                {
                    Console.WriteLine("Person is not a resident. Please try again.");
                }
            }
            Console.WriteLine("Token successfully created.");
        }

        //Basic Featues(6) : Listing Business Locations
        static void ListBusinessLocation(List<BusinessLocation> bList)
        {
            foreach (BusinessLocation b in bList)
            {
                Console.WriteLine("{0} {1} {2}",
                    b.BusinessName, b.BranchCode, b.MaximumCapacity);
            }
        }

        //Basic Featues(7) : Editing Business Location Capacity 
        static void EditBusinessLocationCapacity(List<BusinessLocation> bList)
        {
            BusinessLocation b = InputValidationBusinessLocation(bList);

            while (true)
            {
                Console.Write("Insert new maximum capacity: ");

                try
                {
                    b.MaximumCapacity = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input is not a number. Please try again.");
                }
                if (b.MaximumCapacity < 0)
                {
                    Console.WriteLine("Maximum capacity cannot be negative. Please try again.");
                }
            }
            Console.WriteLine("Business location max capacity successfully edited.");
        }

        //Basic Featues(8) : Safe Entry Check-In 
        static void SafeEntryCheckIn(List<Person> pList, List<BusinessLocation> bList)
        {
            Person p = InputValidationPerson(pList);
            ListBusinessLocation(bList);

            BusinessLocation b = InputValidationBusinessLocation(bList);
            if (b.IsFull() == false)
            {
                b.VisitorsNow++;
                p.AddSafeEntry(new SafeEntry(DateTime.Now,
                    new BusinessLocation(b.BusinessName, b.BranchCode, b.MaximumCapacity)));
                Console.WriteLine("Successfully checked in.");
            }
        }

        //Basic Featues(9) : Safe Entry Check-Out
        static void SafeEntryCheckOut(List<Person> pList)
        {
            Person p = InputValidationPerson(pList);
            foreach (SafeEntry se in p.safeEntryList)
            {
                if (se.CheckOut == default) //if check out is not assigned and given default value
                {
                    Console.WriteLine("{0} {1}", se.CheckIn, se.Location);
                }
            }

            bool locationBool = true;
            while (locationBool)
            {
                Console.Write("Enter business location to check out: ");
                string bLocation = Console.ReadLine();

                foreach (SafeEntry se in p.safeEntryList)
                {
                    if (se.Location.BusinessName == bLocation) //find location's safeentry record
                    {
                        se.PerformCheckOut();
                        Console.WriteLine("Successfully checked out.");
                        locationBool = false;
                    }
                }
                if (locationBool == true)
                {
                    //location not found in safeentry records
                    Console.WriteLine("Business location not found. Please try again.");
                }
            }
        }

        //Basic Featues(10) : Listing SHNFacilities
        static void ListSHNFacilities(List<SHNFacility> sList)
        {
            Console.WriteLine("{0,15}  {1,20}  {2,25}  {3,30}  {4,30}",
                        "Facility Name", "Facility Capacity", "Distance From Air Checkpoint",
                        "Distance From Sea Checkpoint", "Distance From Land Checkpoint");
            foreach (SHNFacility d in sList)
            {
                Console.WriteLine("{0,15}  {1,20}  {2,28}  {3,30}  {4,30}",
                                   d.FacilityName, d.FacilityCapacity, d.DistFromAirCheckpoint,
                                   d.DistFromSeaCheckpoint, d.DistFromLandCheckpoint);
            }
        }

        //Basic Features(11) : Create Visitor
        static void CreateVisitor(List<Person> pList)
        {
            Console.Write("Insert Name: ");
            string name = Console.ReadLine();
            Console.Write("Insert Passport Number: ");
            string passNo = Console.ReadLine();
            Console.Write("Insert nationationality: ");
            string nat = Console.ReadLine();
            Visitor v = new Visitor(name, passNo, nat);
            pList.Add(v);
            Console.WriteLine("Visitor successfully created.");
        }


        //Basic Features(12) : Create TravelEntry Record
        static void CreateTravelEntryRecord(List<Person> pList, List<SHNFacility> sList)
        {
            Person p = InputValidationPerson(pList);

            Console.Write("Insert last country of embarkation: ");
            string lastEmbark = Console.ReadLine();
            Console.Write("Insert entry mode: ");
            string entryMode = Console.ReadLine();  //prompt user for details
            DateTime entryDate = default;

            while (true)
            {
                try
                {
                    Console.Write("Insert Entry Date (dd/mm/yyyy): ");
                    entryDate = DateTime.Parse(Console.ReadLine()); //input validation for datetime
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Date entered was not in the correct format. Please try again.");
                }
            }

            TravelEntry te = new TravelEntry(lastEmbark, entryMode, entryDate);

            te.CalculateSHNDuration(); // calculate SHNEndDate

            //listing SHN facilities for user to select if neccessary
            if (p.travelEntryList != null)
            {
                ListSHNFacilities(sList);

                while (true)
                {
                    Console.Write("Select SHN Facility: ");
                    string shnFacilityName = Console.ReadLine();

                    foreach (SHNFacility shnfac in sList)
                    {
                        if (shnfac.FacilityName == shnFacilityName)
                        {
                            if (shnfac.IsAvailable() == true) // check if shn facility has space
                            {
                                shnfac.FacilityVacancy -= 1;
                                p.AddTravelEntry(te); //adding travel entry 
                                Console.WriteLine("Travel entry successfully created.");
                                return;
                            }
                            else if (shnfac.IsAvailable() == false)
                            {
                                Console.WriteLine("SHN Facility is full, please choose another facility.");
                            }
                        }
                    }
                    Console.WriteLine("SHN Facility not found. Please try again.");
                }
            }
        }

        //Basic Feature (13): Calculate SHN Charges
        static void CalculateSHNCharges(List<Person> pList)
        {
            Person p = InputValidationPerson(pList);

            foreach (TravelEntry te in p.travelEntryList)
            {
                Console.WriteLine(te.ToString());
                Console.WriteLine(te.ShnEndDate);
                if (te.ShnEndDate < DateTime.Now && te.IsPaid == false) //if shn ended and has not been paid
                {
                    double charges = p.CalculateSHNCharges();
                    Console.WriteLine("Please pay $" + charges + " for your swab test and/or SHN stay.");
                    te.IsPaid = true;
                    Console.WriteLine("Payment successful.");
                }
            }
        }

        //Advanced Feature(1) : Contact Tracing Reporting
        static void ContactTracingReporting(List<Person> pList, List<BusinessLocation> bList)
        {
            DateTime time = default;
            while (true)
            {
                try
                {
                    Console.Write("Enter datetime (dd/mm/yyyy): ");
                    time = DateTime.Parse(Console.ReadLine());
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Date entered was not in the correct format. Please try again.");
                }
            }

            BusinessLocation b = InputValidationBusinessLocation(bList);
            List<Person> personCheckInList = new List<Person>();

            //search for people who were at the location during the time given
            foreach (Person p in pList)
            {
                foreach (SafeEntry se in p.safeEntryList)
                {
                    if (se.Location.BusinessName == b.BusinessName
                        && se.CheckIn <= time && time <= se.CheckOut)
                    {
                        personCheckInList.Add(p);
                    }
                }
            }

            //write data to csv file (name, check-in and check-out time, location)
            foreach (Person p in personCheckInList)
            {
                Console.WriteLine(p.Name);
                using (StreamWriter sw = new StreamWriter("ContactTracing.csv", true))
                {
                    foreach (SafeEntry se in p.safeEntryList)
                    {
                        string data = p.Name + ',' + se.CheckIn + ',' + se.CheckOut + ',' + se.Location.BusinessName;
                        sw.WriteLine(data);
                    }
                }
            }
            Console.WriteLine("CSV file successfully generated.");
        }

        //3.2 SHN Status Reporting 
        static void SHNStatusReporting(List<Person> pList)
        {
            DateTime date = default;
            while (true)
            {
                try
                {
                    Console.Write("Input date (dd/mm/yyyy): ");
                    date = DateTime.Parse(Console.ReadLine());
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Date entered was not in the correct format. Please try again.");
                }
            }

            //given a date, search for which travellers currently serving SHN, the SHN EndDate and SHN Location
            //then use streamwriter to make it into a csv
            using (StreamWriter sw = new StreamWriter("SHNStatusReporting.csv", true))
            {
                foreach (Person p in pList)
                {
                    foreach (TravelEntry te in p.travelEntryList)
                    {
                        if (te.EntryDate < date && te.ShnEndDate > date)
                        {
                            string data = p.Name + ',' + te.ShnEndDate + ',' + te.ShnStay;
                        }
                    }
                }
            }
            Console.WriteLine("CSV file successfully generated.");
        }

        //3.3 SHN Country Reporting (?)
        static void SHNCountryReporting(List<Person> pList)
        {
            Console.Write("Input last country of embarkation: ");
            string lastCountry = Console.ReadLine();
            string shnStatus = null;

            //given a country, search for which travellers currently serving SHN, the SHN EndDate and SHN Location
            //then use streamwriter to make it into a csv
            using (StreamWriter sw = new StreamWriter("CountryReporting.csv", true))
            {
                foreach (Person p in pList)
                {
                    foreach (TravelEntry te in p.travelEntryList)
                    {
                        if (te.EntryDate < DateTime.Now && te.ShnEndDate > DateTime.Now)
                        {
                            if (te.ShnEndDate <= DateTime.Now)
                            {
                                shnStatus = "ended";
                            }
                            else if (te.ShnEndDate > DateTime.Now)
                            {
                                shnStatus = "ongoing";
                            }
                            string data = p.Name + ',' + te.LastCountryOfEmbarkation + ',' + te.ShnStay + ',' + shnStatus + ',' + te.ShnEndDate;
                        }
                    }
                }
            }
            Console.WriteLine("CSV file successfully generated.");
        }

        //input validation methods
        static Person InputValidationPerson(List<Person> pList)
        {
            while (true)
            {
                Console.Write("Enter name: ");
                string name = Console.ReadLine();

                foreach (Person p in pList)
                {
                    if (p.Name == name)
                    {
                        return p;
                    }
                }
                Console.WriteLine("Person not found. Please try again.");
            }
        }

        static BusinessLocation InputValidationBusinessLocation(List<BusinessLocation> bList)
        {
            while (true)
            {
                Console.Write("Enter business location name: ");
                string name = Console.ReadLine();

                foreach (BusinessLocation bLocation in bList)
                {
                    if (bLocation.BusinessName == name)
                    {
                        return bLocation;
                    }
                }
                Console.WriteLine("Business location not found. Please try again.");
            }
        }
    }
}
