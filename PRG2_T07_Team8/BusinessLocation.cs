﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class BusinessLocation
    {
        private string businessName;
        public string BusinessName
        {
            get { return businessName; }
            set { businessName = value; }
        }

        private string branchCode;
        public string BranchCode
        {
            get { return branchCode; }
            set { branchCode = value; }
        }

        private int maximumCapacity;
        public int MaximumCapacity
        {
            get { return maximumCapacity; }
            set { maximumCapacity = value; }
        }

        private int visitorsNow;
        public int VisitorsNow
        {
            get { return visitorsNow; }
            set { visitorsNow = value; }
        }

        public BusinessLocation() { }

        public BusinessLocation(string bn, string bc, int mc)
        {
            BusinessName = bn;
            BranchCode = bc;
            MaximumCapacity = mc;
        }

        public bool IsFull()
        {
            if (VisitorsNow >= MaximumCapacity)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return "Business name: " + BusinessName
                + "\tBranch Code: " + BranchCode
                + "\tMaximum capacity: " + MaximumCapacity;
        }
    }
}
