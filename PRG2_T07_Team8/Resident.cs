﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class Resident : Person
    {
        //attributes
        private string address;
        private DateTime lastLeftCountry;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public DateTime LastLeftCountry
        {
            get { return lastLeftCountry; }
            set { lastLeftCountry = value; }
        }

        public TraceTogetherToken Token { get; set; }

        //constructors
        public Resident() : base() { }

        public Resident(string n, string a, DateTime d) : base(n)
        {
            Address = a;
            LastLeftCountry = d;
        }

        //methods
        //overiding abstract method
        public override double CalculateSHNCharges() //fix later
        {
            foreach (TravelEntry te in travelEntryList)
            {
                te.CalculateSHNDuration();
                int shnTime = te.ShnEndDate.Subtract(te.EntryDate).Days;
                Console.WriteLine(shnTime);
                if (shnTime == 0)
                    return 200;
                else if (shnTime == 7)
                {
                    return 220 * 1.07;
                }
                else if (shnTime == 14)
                    return 1220 * 1.07;
            }
            return 0;
        }

        public override string ToString()
        {
            return base.ToString() + 
                "\tAddress: " + Address + 
                "\tLast Left Country: " + LastLeftCountry;
        }
    }
}

