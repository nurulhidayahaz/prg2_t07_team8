﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class SHNFacility
    {
        private string facilityName;
        public string FacilityName
        {
            get { return facilityName; }
            set { facilityName = value; }
        }

        private int facilityCapacity;
        public int FacilityCapacity
        {
            get { return facilityCapacity; }
            set { facilityCapacity = value; }
        }

        private int facilityVacancy;
        public int FacilityVacancy
        {
            get { return facilityVacancy; }
            set { facilityVacancy = value; }
        }

        private double distFromAirCheckpoint;
        public double DistFromAirCheckpoint
        {
            get { return distFromAirCheckpoint; }
            set { distFromAirCheckpoint = value; }
        }

        private double distFromSeaCheckpoint;
        public double DistFromSeaCheckpoint
        {
            get { return distFromSeaCheckpoint; }
            set { distFromSeaCheckpoint = value; }
        }

        private double distFromLandCheckpoint;
        public double DistFromLandCheckpoint
        {
            get { return distFromLandCheckpoint; }
            set { distFromLandCheckpoint = value; }
        }

        public SHNFacility() { }

        public SHNFacility(string fn, int fc, double da, double ds, double dl)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            DistFromAirCheckpoint = da;
            DistFromSeaCheckpoint = ds;
            DistFromLandCheckpoint = dl;
        }

        public double CalculateTravelCost(string entryMode, DateTime entryDate)
        {
            double fare = 0;
            // calculate fare based on entryMode selected
            if (entryMode.ToLower() == "air")
            {
                fare = 50 + (0.22 * DistFromAirCheckpoint);
            }
            else if (entryMode.ToLower() == "sea")
            {
                fare = 50 + (0.22 * DistFromSeaCheckpoint);
            }
            else if (entryMode.ToLower() == "land")
            {
                fare = 50 + (0.22 * DistFromLandCheckpoint);
            }

            // if entryDate is between 6am to 8:59am or 6pm to 23:59pm = 25% surcharge
            // else, if entryDate is between 12am to 5:59am = 50% surcharge
            if ((entryDate.TimeOfDay >= TimeSpan.Parse("06:00:00") && entryDate.TimeOfDay <= TimeSpan.Parse("08:59:00"))
                || (entryDate.TimeOfDay >= TimeSpan.Parse("18:00:00") && entryDate.TimeOfDay <= TimeSpan.Parse("23:59:00")))
            {
                fare += fare * 0.25;
            }
            else if (entryDate.TimeOfDay >= TimeSpan.Parse("00:00:00") && entryDate.TimeOfDay <= TimeSpan.Parse("05:59:00"))
            {
                fare += fare * 0.5;
            }
            return fare;
        }

        public bool IsAvailable()
        {
            if (FacilityCapacity - FacilityVacancy != 0) //  check for any available slots left
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return "Facility name: " + FacilityName
                + "\tFacility Capacity: " + FacilityCapacity
                + "\tDistance from air checkpoint: " + DistFromAirCheckpoint
                + "\tDistance from sea checkpoint: " + DistFromSeaCheckpoint
                + "\tDistance from land checkpoint: " + DistFromLandCheckpoint;
        }
    }
}
