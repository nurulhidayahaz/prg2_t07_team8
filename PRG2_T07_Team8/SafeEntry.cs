﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class SafeEntry
    {
        private DateTime checkIn;
        public DateTime CheckIn
        {
            get { return checkIn; }
            set { checkIn = value; }
        }

        private DateTime checkOut;
        public DateTime CheckOut
        {
            get { return checkOut; }
            set { checkOut = value; }
        }

        private BusinessLocation location;
        public BusinessLocation Location
        {
            get { return location; }
            set { location = value; }
        }

        public SafeEntry() { }

        public SafeEntry(DateTime ci, BusinessLocation l)
        {
            CheckIn = ci;
            Location = l;
        }

        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
            Location.VisitorsNow -= 1;
        }

        public override string ToString()
        {
            return "Check In: " + CheckIn
                + "\tLocation: " + Location;
        }
    }
}
