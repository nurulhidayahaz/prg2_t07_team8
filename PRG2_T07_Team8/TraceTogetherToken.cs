﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class TraceTogetherToken
    {
        //attributes
        private string serialNo;
        private string collectionLocation;
        private DateTime expiryData;
        public string SerialNo
        {
            get { return serialNo; }
            set { serialNo = value; }
        }
        public string CollectionLocation
        {
            get { return collectionLocation; }
            set { collectionLocation = value; }
        }
        public DateTime ExpiryDate
        {
            get { return expiryData; }
            set { expiryData = value; }
        }

        //constructor
        public TraceTogetherToken() { }
        public TraceTogetherToken(string sNo, string colLoc, DateTime expDate)
        {
            SerialNo = sNo;
            CollectionLocation = colLoc;
            ExpiryDate = expDate;
        }

        //methods
        public bool IsEligibleForReplacement()
        {
            DateTime currentDate = DateTime.Now;
            int diff = ExpiryDate.Subtract(currentDate).Days;
            if (diff <= 30)
            {
                return true;
            }
            else
                return false;
        }

        public void ReplaceToken(string sNo, string colLoc)
        {
            SerialNo = sNo;
            CollectionLocation = colLoc;
        }

        public override string ToString()
        {
            return "Serial No: " + SerialNo + 
                "\tCollection Location: " + CollectionLocation + "\tExpiry Date: " + ExpiryDate;
        }
    }
}
