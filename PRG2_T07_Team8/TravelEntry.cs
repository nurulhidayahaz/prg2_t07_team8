﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class TravelEntry
    {
        private string lastCountryOfEmbarkation;
        public string LastCountryOfEmbarkation
        {
            get { return lastCountryOfEmbarkation; }
            set { lastCountryOfEmbarkation = value; }
        }

        private string entryMode;
        public string EntryMode
        {
            get { return entryMode; }
            set { entryMode = value; }
        }

        private DateTime entryDate;
        public DateTime EntryDate
        {
            get { return entryDate; }
            set { entryDate = value; }
        }

        private DateTime shnEndDate;
        public DateTime ShnEndDate
        {
            get { return shnEndDate; }
            set { shnEndDate = value; }
        }

        private SHNFacility shnStay;
        public SHNFacility ShnStay
        {
            get { return shnStay; }
            set { shnStay = value; }
        }

        private bool isPaid;
        public bool IsPaid
        {
            get { return isPaid; }
            set { isPaid = value; }
        }

        public TravelEntry() { }

        public TravelEntry(string lc, string em, DateTime ed)
        {
            LastCountryOfEmbarkation = lc;
            EntryMode = em;
            EntryDate = ed;
        }

        public void CalculateSHNDuration()
        {
            int duration = 0;
            if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                duration = 0;
            }
            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                duration = 7;
            }
            else
            {
                duration = 14;
            }

            ShnEndDate = EntryDate.AddDays(duration);
        }

        public void AssignSHNFacility(SHNFacility shnf)
        {
            ShnStay = shnf;
            ShnStay.FacilityVacancy -= 1;
        }

        public override string ToString()
        {
            return "Last country of embarkation: " + LastCountryOfEmbarkation
                + "\tEntry Mode: " + EntryMode
                + "\tEntry Date: " + EntryDate;
        }
    }
}
