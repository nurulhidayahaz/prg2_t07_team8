﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
    class Visitor : Person
    {
        //attributes
        private string passportNo;
        private string nationality;

        public string PassportNo
        {
            get { return passportNo; }
            set { passportNo = value; }
        }
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }

        //constructors
        public Visitor() : base() { }

        public Visitor(string n, string pNo, string nat) : base(n)
        {
            PassportNo = pNo;
            Nationality = nat;
        }

        //methods
        //overiding abstract method
        public override double CalculateSHNCharges()
        {
            foreach (TravelEntry te in travelEntryList)
            {
                te.CalculateSHNDuration();
                int shnTime = te.ShnEndDate.Subtract(te.EntryDate).Days;
                Console.WriteLine(shnTime);
                if (shnTime == 0 || shnTime == 7)
                    return 280 * 1.07;
                else if (shnTime == 14)
                    return (2000 + te.ShnStay.CalculateTravelCost(te.EntryMode, te.EntryDate)) * 1.07;
            }
            return 0;

        }

        public override string ToString()
        {
            return base.ToString() + 
                "\tPassport No: " + PassportNo + 
                "\tNationality: " + Nationality;
        }
    }

}
