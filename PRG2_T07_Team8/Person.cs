﻿//============================================================
// Student Number : S10204622, S10206967
// Student Name : Nur Adeelya Bte Muhammad Hidhir, Nurul Hidayah Bte Mohamad Azmi
// Module Group : T07
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T07_Team8
{
	abstract class Person
	{
        //attributes
        private string name;

        //attributes
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public List<SafeEntry> safeEntryList { get; set; }
        public List<TravelEntry> travelEntryList { get; set; }

        //constructor
        public Person()
        {
            safeEntryList = new List<SafeEntry>();
            travelEntryList = new List<TravelEntry>();
        }
        public Person(string n)
        {
            Name = n;
            safeEntryList = new List<SafeEntry>();
            travelEntryList = new List<TravelEntry>();
        }

        //methods
        public void AddSafeEntry(SafeEntry sEntry)
        {
            safeEntryList.Add(sEntry);
        }

        public void AddTravelEntry(TravelEntry tEntry)
        {
            travelEntryList.Add(tEntry);
        }

        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            return "Name: " + Name;
        }
    }
}
